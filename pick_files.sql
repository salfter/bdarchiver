/*!50003 DROP PROCEDURE IF EXISTS `pick_files` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`salfter`@`%` PROCEDURE `pick_files`(size bigint)
BEGIN
    declare largest int;
    declare largest_size bigint;
    l: loop
        set largest=null;
        select id, filesize into largest, largest_size from backup_index where discnum is null and filesize<=size*2048 order by filesize desc limit 1;
        if largest is null
        then
            leave l;
        end if;
        update backup_index set discnum=-1 where id=largest;
        set size=size-truncate((largest_size-1)/2048+1, 0);
    end loop l;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

